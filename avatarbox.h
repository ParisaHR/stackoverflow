#ifndef AVATARBOX_H
#define AVATARBOX_H


#include <QFrame>
#include <QLabel>

namespace Ui {
class AvatarBox;
}

class AvatarBox : public QFrame
{
    Q_OBJECT

public:
    explicit AvatarBox(QWidget *parent = 0);
    ~AvatarBox();

    void setOnline( bool online);
private:
    Ui::AvatarBox *ui;
    QLabel *labelOnline;
};
#endif // AVATARBOX_H
