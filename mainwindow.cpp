#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    //    slotSetOnline();    //comment 1

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::slotSetOnline(bool flag)
{
    ui->avatar->setOnline( flag );
}


void MainWindow::on_pushButton_clicked(bool checked)
{

    slotSetOnline(checked);

}

