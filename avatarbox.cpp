#include "avatarbox.h"
#include "ui_avatarbox.h"

AvatarBox::AvatarBox(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::AvatarBox)
{
    ui->setupUi(this);
    int size = 20;

    labelOnline = new QLabel(this);
    labelOnline->setScaledContents( true );
    labelOnline->setPixmap( QPixmap(":/icon/online.png") );
    labelOnline->setFixedSize( size, size);
    labelOnline->move( ui->labelAvatar->width()- size -2,ui->labelAvatar->height()+(size*2.3));
    labelOnline->hide();
}

AvatarBox::~AvatarBox()
{
    delete ui;
}

void AvatarBox::setOnline(bool online)
{
    if( online )
    {
        labelOnline->show();

    }
    else
    {
        labelOnline->hide();

    }
}
